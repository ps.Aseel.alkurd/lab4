package com.company;

import java.util.Scanner;
import java.util.Stack;

public class EquationProcessor {
    final static String validOperandsAndOperationInFirstChar = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVXYZ0123456789(-";
    final static String validOperandsAndOperationInLastChar = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVXYZ0123456789)";
    final static String variables = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVXYZ";
    final static String operandsAndOperation = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVXYZ0123456789*+-/)(^";
    final static String operations = "*/+-^";
    static Stack<String> operationStack = new Stack();
    static Stack<String> operandsStack = new Stack(); // variables and numbers
    static String tempVarForExistingVar = "";
    static String valueForExistingVar = "";

    public static double solveEquation(String equation) {
        verifyValidityOfEquation(equation);
        char[] equationAsChars = equation.toCharArray();

        for (int i = 0; i < equationAsChars.length; i++) {
            if (equationAsChars[i] == ' ')
                continue;


            if (equationAsChars[i] >= '0' && equationAsChars[i] <= '9' || (i==0 && equationAsChars[0] == '-')) { // if number push it to the operands stack
                // like string but here i can use append method
                StringBuilder stringBuilder = new StringBuilder();
                // There may be more than one digit in number
                while (i < equationAsChars.length && equationAsChars[i] >= '0' && equationAsChars[i] <= '9' || (i==0 && equationAsChars[0] == '-')){
                    if(equationAsChars[0] == '-' && i == 0){
                        stringBuilder.append('-');
                        i++;

                    }else
                    stringBuilder.append(equationAsChars[i++]);
                }
                operandsStack.push((stringBuilder.toString()));
                i--;

            } else if (equationAsChars[i] == '(')
                operationStack.push(String.valueOf(equationAsChars[i]));

                // Closing brace encountered,
                // solve entire brace
            else if (equationAsChars[i] == ')') {
                if (!operationStack.peek().equals('('))
                    operandsStack.push(doOperation(operationStack.pop(), operandsStack.pop(), operandsStack.pop()));
                operationStack.pop();
            } else if (variables.contains(String.valueOf(equationAsChars[i]))) { // if variables push it to operands stack
                operandsStack.push(String.valueOf(equationAsChars[i]));
            } else if (equationAsChars[i] == '+' || equationAsChars[i] == '-' || equationAsChars[i] == '*' || equationAsChars[i] == '^'|| equationAsChars[i] == '/')//push operation to operation stack
            {
                /* while top of operationStack has same or greater precedence to current char in equation
                   which is an operator Apply operator on top of operationStack to top two elements in values stack*/
                while ((!operationStack.empty()) && hasPrecedence(String.valueOf(equationAsChars[i]), operationStack.peek())) {
                    operandsStack.push(doOperation(operationStack.pop(), operandsStack.pop(), operandsStack.pop()));

                }
                operationStack.push(String.valueOf(equationAsChars[i]));


            }

        }
        while (!operationStack.empty())
            operandsStack.push(doOperation(operationStack.pop(), operandsStack.pop(), operandsStack.pop()));

        return Double.parseDouble(operandsStack.pop());
    }

    public static boolean hasPrecedence(String operation1, String operation2) { // returns true if op2 has precedence

        char[] op1 = operation1.toCharArray();
        char[] op2 = operation2.toCharArray();
        if (op2[0] == '(' || op2[0] == ')')
            return false;
        if ((op1[0] == '^' || op1[0] == '*' || op1[0] == '/') && (op2[0] == '+' || op2[0] == '-'))
            return false;
        else
            return true;
    }

    public static String doOperation(String op, String b, String a) {
        if(tempVarForExistingVar.equalsIgnoreCase( a) || tempVarForExistingVar.equalsIgnoreCase( b)  ){
            if(tempVarForExistingVar.equalsIgnoreCase( a)){
                a = valueForExistingVar;
            }else if(tempVarForExistingVar.equalsIgnoreCase( b)){
                b = valueForExistingVar;
            }
        } if (variables.contains(a) || variables.contains(b)) {
            int varValue = 0;
            if (variables.contains(a)) {
                a =  enterVariable(a);
                valueForExistingVar = (a);
            }
            if (variables.contains(b)) {
                b =  enterVariable(b);
                valueForExistingVar = b;
            }
        }
        switch (op) {
            case "+":
                return String.valueOf(Integer.parseInt(a) + Integer.parseInt(b));
            case "-":
                return String.valueOf(Integer.parseInt(a) - Integer.parseInt(b));
            case "*":
                return String.valueOf(Integer.parseInt(a) * Integer.parseInt(b));
            case "^":
                return String.valueOf(Math.pow(Integer.parseInt(a),Integer.parseInt(b)));
            case "/":
                if (Integer.parseInt(b) == 0)
                    throw new UnsupportedOperationException("Cannot divide by zero");
                return String.valueOf(Integer.parseInt(a) / Integer.parseInt(b));
        }

        return String.valueOf(0);
    }
    static String enterVariable(String var){
        int varValue;
        tempVarForExistingVar = var;
        Scanner scanner = new Scanner(System.in);
        System.out.println("please enter the value for: " + var);
        varValue = scanner.nextInt();
        var = String.valueOf(varValue);
        return var;
    }

    public static void verifyValidityOfEquation(String equation) {
        char[] tempEquation = equation.toCharArray();
        if (!(isValidFirstChar(tempEquation[0]) && isValidLastChar(tempEquation[tempEquation.length - 1]))) {// if start or end with symbol
            throw new MyException("The equation starts or ends with invalid char");
        }
        if (!isEquationContainUnValidChar(tempEquation)) { // if contains invalid char
            throw new MyException("The equation contains invalid char ");
        }
        if ((isEquationContainsMoreThanOneSymbolBehindEachOther(tempEquation))) {
            throw new MyException("invalid equation Structure");
        }


    }

    public static boolean isEquationContainsMoreThanOneSymbolBehindEachOther(char[] equation) {
        boolean check = false;
        for (int i = 0; i < equation.length; i++) {
            if (operations.contains(String.valueOf(equation[i]))) {
                if (i == equation.length - 1)
                    return check = false; // weslna to the end o ma fe eshe mkarar
                if (operations.contains(String.valueOf(equation[i + 1]))) {
                    return check = true; // enha contain
                }
            }
        }
        return check;
    }

    public static boolean isEquationContainUnValidChar(char[] equation) {
        boolean check = false;
        for (char c : equation) {
            if (operandsAndOperation.contains(String.valueOf(c))) {
                check = true;
            } else {
                return false;
            }
        }
        return check;
    }

    public static boolean isValidFirstChar(char firstChar) { // true means valid first char
        if(validOperandsAndOperationInFirstChar.contains(String.valueOf(firstChar)))
            return true;
        return false;

    }

    public static boolean isValidLastChar(char lastChar) { // true means valid last char
        if(validOperandsAndOperationInLastChar.contains(String.valueOf(lastChar)))
            return true;
        return false;
    }
}